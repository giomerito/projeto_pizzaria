import { Router, Request, Response } from 'express';
import multer from 'multer';

import { AuthUserController } from './controllers/user/AuthUserController';
import { CreateUserController } from './controllers/user/CreateUserController';
import { DetailUserController } from './controllers/user/DetailUserController';
import { CreateCategoryController } from './controllers/category/CreateCategoryController';
import { ListcategoryController } from './controllers/category/ListCategoryController';

import { isAuthenticated } from './middlewares/isAuthenticated';
import { CreateProductController } from './controllers/product/CreateProductController';
import { ListByCategoryController } from './controllers/product/ListByCategoryController';
import { CreateOrderController } from './controllers/order/CreateOrderController';


import uploadConfig from '../src/config/multer';
import { RemoveOrderController } from './controllers/order/RemoveOrderController';
import { AddItemController } from './controllers/order/AddItemController';
import { RemoveItemController } from './controllers/order/RemoveItemController';
import { SendOrderController } from './controllers/order/SendOrderController';
import { ListOrderController } from './controllers/order/ListOrderController';
import { DetailOrderController } from './controllers/order/DetailOrderController';
import { FinishOrderController } from './controllers/order/FinishOrderController';

const router = Router();

/** Configuração para fazer upload de fotos */

const upload = multer(uploadConfig.upload("./tmp"));

/** Fim da configuração do uploads das imagens */

// -- ROTAS USER --
router.post('/users', new CreateUserController().handle);
//Login no sistema
router.post('/session', new AuthUserController().handle);
//buscar dados do usuario
router.get('/me', isAuthenticated, new DetailUserController().handle);


/** Rotas para CATEGORIAS */
router.post('/category', isAuthenticated, new CreateCategoryController().handle);
router.get('/category', isAuthenticated, new ListcategoryController().handle);

/** Rotas para Produtos */
router.post('/product', isAuthenticated, upload.single('file'), new CreateProductController().handle);

//Listando produtos por categoria
router.get('/category/products', isAuthenticated, new ListByCategoryController().handle);

//Rotas das orders
router.post('/order', isAuthenticated, new CreateOrderController().handle);
router.delete('/order', isAuthenticated, new RemoveOrderController().handle);

//Rotas para adicionar items
router.post('/order/add', isAuthenticated, new AddItemController().handle);

//rotas remove item de uma mesa
router.delete('/order/remove', isAuthenticated, new RemoveItemController().handle);

//rotas para finalizar pedido e enviar para cozinha
router.put('/order/send', isAuthenticated, new SendOrderController().handle);
//Listar ordens enviadas
router.get('/order/list', isAuthenticated, new ListOrderController().handle);

//detalhes do pedido
router.get('/order/detail', isAuthenticated, new DetailOrderController().handle);

//rota para finalizar pedido na cozinha
router.put('/order/finish', isAuthenticated, new FinishOrderController().handle);

export { router };