import {Request, Response} from 'express';
import { ListCategoryService } from '../../services/category/ListCategoryService';


class ListcategoryController{
    async handle(req: Request, res: Response){

        const listCategoryController = new ListCategoryService();

        const listCategory = await listCategoryController.execute();

        return res.json(listCategory);
    }
}

export { ListcategoryController }