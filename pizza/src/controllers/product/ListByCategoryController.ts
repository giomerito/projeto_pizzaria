import {Request, Response} from 'express';
import { ListCategoryService } from '../../services/category/ListCategoryService';
import { ListByCategoryService } from '../../services/product/ListBycategoryService';


class ListByCategoryController{
    async handle(req: Request, res: Response){
        const category_id = req.query.category_id as string;

        const listByCategory = new ListByCategoryService();

        const products = await listByCategory.execute({
            category_id
        });

        return res.json(products);
    }
}

export { ListByCategoryController }