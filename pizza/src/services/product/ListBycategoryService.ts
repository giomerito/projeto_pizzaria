import prismaClient from "../../prisma";

interface Productrequest{
    category_id: string;
}

class ListByCategoryService{
    async execute({category_id}: Productrequest){
        const findByCategory = await prismaClient.product.findMany({
            where:{
                category_id: category_id
            }
        })

        return findByCategory;
    }
}

export { ListByCategoryService }