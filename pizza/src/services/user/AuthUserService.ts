import { compare } from "bcryptjs";
import prismaClient from "../../prisma";
import { sign } from 'jsonwebtoken';

interface AuthRequest{
    email: string;
    password: string;
}

class AuthUserService{
    async execute({email, password}: AuthRequest ){
        const user = await prismaClient.user.findFirst({ 
            where: {email: email}
        })
        if(!user) throw new Error("user/password incorrect");

        //para verificar se a senha recebida esta correta
        const passwordMath = await compare(password, user.password);
        if(!passwordMath) throw new Error("user/password incorrect");
        

        //Gerar um token JWT para devolver os dados do usuário como id, name e email
        const token = sign(
            {
                name: user.name,
                email: user.email
            },
            process.env.JWT_SECRET,
            {
                subject: user.id,
                expiresIn: "30d"
            }
        );

        return { 
            id: user.id,
            name: user.name,
            email: user.email,
            token: token
         }
    }



    
}

export { AuthUserService }