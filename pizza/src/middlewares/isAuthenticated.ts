import {NextFunction, Request, Response} from 'express';
import { verify } from 'jsonwebtoken';

interface PayLoad{
    sub: string
}


export function isAuthenticated(req: Request, res: Response, next: NextFunction){
    
    //Receber o token do usuário logado
    const authToken = req.headers.authorization;

    if(!authToken) return res.status(401).end();

    const [, token] = authToken.split(" ");

    try{
        //Validar o token
        const { sub } = verify(
            token,
            process.env.JWT_SECRET
        ) as PayLoad;
        
        //recuperar o id do token e colocado dentro da variavel user_id dentro do Request
        req.user_id = sub;
        next();
        
    }catch(err){
        return res.status(401).end();
    }

} 